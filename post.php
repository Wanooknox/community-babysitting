<?php ob_start(); ?>
<h2>POST A JOB</h2>

<?php
    if (isset($_COOKIE['parent']))
    {
        $user = unserialize($_COOKIE['parent']);
    }
    if (isset($user) && $user->usertype === 'p')
    {
        $user->FetchChildren();
        if (count($user->children) > 0)
        {
            if (!isset($_REQUEST['parentid']))
            {
                $frmJob = "<form id='frmJob' name='frmJob' method='POST' action='post.php'><table>";
                $frmJob .= "<input type='hidden' name='parentid' id='parentid' value='". $user->uid ."' />";
                $frmJob .= "<tr><td><label for='day'>Day</label></td><td><select id='day' name='day' >";
                for ($i = 0; $i < 15; $i++)
                {
                    $d = @date("Y-m-d", time()+86400*$i);
                    $dp = @date("F jS, Y", time()+86400*$i);
                    $frmJob .= "<option value='$d'>$dp</option>";
                }
                $frmJob .= "</select></td></tr>";
                $frmJob .= "<tr><td><label for='start'>Start Time</label></td><td><select id='start' name='start' >";
                for ($i = 0; $i < 24; $i++)
                {
                    $s = @date("H:i", strtotime("00:00:00")+3600*$i);
                    $sp = @date("g:i a", strtotime("00:00:00")+3600*$i);
                    $frmJob .= "<option value='$s'>$sp</option>";
                }
                $frmJob .= "</select></td></tr>";
                $frmJob .= "<tr><td><label for='end'>End Time</label></td><td><select id='end' name='end' >";
                for ($i = 0; $i < 24; $i++)
                {
                    $s = @date("H:i", strtotime("00:00:00")+3600*$i);
                    $sp = @date("g:i a", strtotime("00:00:00")+3600*$i);
                    $frmJob .= "<option value='$s'>$sp</option>";
                }
                $frmJob .= "</select></td></tr>";
                $frmJob .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Post' /></td></tr>";
                $frmJob .= "</table></form>";
                echo $frmJob;
            }
            else
            {
                $parent = $_REQUEST['parentid'];
                $day = $_REQUEST['day'];
                $start = $_REQUEST['start'];
                $end = $_REQUEST['end'];
                
                $job = new Job(null, $parent, $day, $start, $end);
                
                $didpost = $job->PostJob();
                if ($didpost > 0)
                {
                    echo "<p>Job posted.</p>";
                }
                else
                {
                    echo "<p>Job post failed.</p>";
                }
            }
        }
        else
        {
            echo "<p class='err'>You must have at least one child before you can post a job. Go to your account settings and add a child.</p>";
        }
    }
    else
    {
        echo "<p class='err'>You must be logged in as a parent to use this page</p>";
    }
?>

<?php
    function __autoload($sClassName)
    {
        require_once("classes/$sClassName.class.php");        
    }
?>
<?php
    $sTitle = "Post Job";
    
    require_once("css/wrapper.php");
?>
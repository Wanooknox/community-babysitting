<?php ob_start(); ?>


<?php
// if not logging out
if (!isset($_REQUEST['logout']))
{
    echo "<h2>LOGIN</h2>";
    //if the email address has not been entered
    if (!isset($_REQUEST['uname']))
    {
	//display the login form
	$frmLogin = "<form id='frmLogin' name='frmLogin' method='POST'><table>";
	$frmLogin .= "<tr><td><label for='uname'>Username </label></td> <td><input type='text' id='uname' name='uname' /></td></tr>";
	$frmLogin .= "<tr><td><label for='pass'>Password </label></td> <td><input type='password' id='pass' name='pass' /></td></tr>";
	$frmLogin .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Login' /></td></tr>";
	$frmLogin .= "</table></form>";
	echo $frmLogin;  
    }
    else //the email has been passed assuming form submitted successfully
    {
        $uname = $_REQUEST['uname'];
        $pass = $_REQUEST['pass'];
        
        $loginuser = new User($uname, $pass);
        $isloggedin = $loginuser->Login();
        if($isloggedin === true)
        {
            echo "<p>Successfully logged in!</p>";
            if (isset($_COOKIE['loginback']))
            {
                $goto = $_COOKIE['loginback'];
                setcookie("loginback", "", 0);
                header("Location: $goto");
            }
        }
        else
        {
            echo $isloggedin;
        }
        
    }
}
else //use the page to log the current user out
{
    echo "<h2>LOGOUT</h2>";
    if (isset($_COOKIE['parent']))
    {
        $user = unserialize($_COOKIE['parent']);
    }
    elseif (isset($_COOKIE['sitter']))
    {
        $user = unserialize($_COOKIE['sitter']);
    }
    
    echo "<p>Goodbye, " . $user->name . ". You are now logged out.</p>";
    $user->Logout();
}
?>

<?php
    function __autoload($sClassName)
    {
        require_once("classes/$sClassName.class.php");        
    }
?>
<?php
    $sTitle = "Login";
    
    require_once("css/wrapper.php");
?>
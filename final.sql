delimiter $$

CREATE TABLE `finParents` (
  `parentID` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY  (`parentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

CREATE TABLE `finChildren` (
  `childID` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `age` int(11) NOT NULL,
  `parentID` varchar(20) NOT NULL,
  PRIMARY KEY  (`childID`),
  KEY `fk_finChildren_1` (`parentID`),
  CONSTRAINT `fk_finChildren_1` FOREIGN KEY (`parentID`) REFERENCES `finParents` (`parentID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

CREATE TABLE `finSitters` (
  `sitterID` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `trained` int(1) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY  (`sitterID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

CREATE TABLE `finJobs` (
  `jobID` int(11) NOT NULL auto_increment,
  `parentID` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  PRIMARY KEY  (`jobID`),
  KEY `fk_finJobs_1` (`parentID`),
  CONSTRAINT `fk_finJobs_1` FOREIGN KEY (`parentID`) REFERENCES `finParents` (`parentID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

CREATE TABLE `finSitterViews` (
  `viewID` int(11) NOT NULL auto_increment,
  `sitterID` varchar(20) NOT NULL,
  `parentID` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY  (`viewID`),
  KEY `fk_finSitterViews_1` (`sitterID`),
  KEY `fk_finSitterViews_2` (`parentID`),
  CONSTRAINT `fk_finSitterViews_1` FOREIGN KEY (`sitterID`) REFERENCES `finSitters` (`sitterID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_finSitterViews_2` FOREIGN KEY (`parentID`) REFERENCES `finParents` (`parentID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$



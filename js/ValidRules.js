jQuery.validator.addMethod("phone", function(phone_number, elem) {
            return this.optional(elem) || phone_number.match(/^(\(?\d{3}\)?)?\s?\d{3}[ -]?\d{4}$/);
        }, "Invalid Phone Number");
        
        jQuery.validator.addMethod("postalcode", function(postal_code, elem) {
            return this.optional(elem) || postal_code.match(/^[A-Z]?\d?[A-Z]?[ -]+\d?[A-Z]?\d?$/);
        }, "Invalid Postal Code");
        
jQuery.validator.addMethod("sin", function(sSin, elem) {
            
            sSin = sSin.replace(/-/g,"");
            sSin = sSin.replace(/ /g,"");
            
            return this.optional(elem) || sSin.match(/^\d{9}$/);
        }, "Invalid SIN");

jQuery.validator.addMethod("hosp", function(sHN, elem) {
            
            sHN = sHN.replace(/-/g,"");
            sHN = sHN.replace(/ /g,"");
            
            return this.optional(elem) || sHN.match(/^\d{9}$/);
        }, "Invalid HN");

jQuery.validator.addMethod("creditcard", function(sCc, elem) {
            
            sCc = sCc.replace(/-/g,"");
            sCc = sCc.replace(/ /g,"");
            
            return this.optional(elem) || sCc.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/);
        }, "Invalid Credit Card number");

jQuery.validator.addMethod("cctype", function(sCt, elem) {
            
            sCt = sCt.replace(/-/g,"");
            sCt = sCt.replace(/ /g,"");
            
            return this.optional(elem) || sCt.match(/^[1-9]$/);
        }, "Invalid Credit Card type");

jQuery.validator.addMethod("price", function(op, elem) {
            op = op.replace(/-/g,"");
            op = op.replace(/ /g,"");
            
            return this.optional(elem) || op.match(/^[0-9]+(\.[0-9]{2})$/);
            
}, "Invalid Price");

jQuery.validator.addMethod("qty", function(qty, elem) {
            qty = qty.replace(/-/g,"");
            qty = qty.replace(/ /g,"");
            
            return this.optional(elem) || qty.match(/^\d*$/);
            
}, "Invalid number");

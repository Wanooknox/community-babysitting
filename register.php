<?php ob_start(); ?>
<h2>REGISTER</h2>

<?php
if (!isset($_REQUEST['uname']))
{
    if (isset($_REQUEST['type']))
    {
        $usertype = $_REQUEST['type'];
        if ($usertype === 'p')
        {
            echo "<p>Welcome new parent, please enter your information below.</p>";
            
            $frmParent = "<form id='frmParent' name='frmParent' method='POST' action='register.php'><table>";
            $frmParent .= "<input type='hidden' name='type' id='type' value='p' />";
            $frmParent .= "<tr><td><label for='name'>Name</label></td><td><input type='text' id='name' name='name' /></td></tr>";
            $frmParent .= "<tr><td><label for='phone'>Phone</label></td><td><input type='text' id='phone' name='phone' /></td></tr>";
            $frmParent .= "<tr><td><label for='email'>Email</label></td><td><input type='text' id='email' name='email' /></td></tr>";
            $frmParent .= "<tr><td colspan='2'>Please choose a username and password</td></tr>";
            $frmParent .= "<tr><td><label for='uname'>Username</label></td><td><input type='text' id='uname' name='uname' /></td></tr>";
            $frmParent .= "<tr><td><label for='pass'>Password</label></td><td><input type='password' id='pass' name='pass' /></td></tr>";
            $frmParent .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Register' /></td></tr>";
            $frmParent .= "</table></form>";
            echo $frmParent;
            
        }
        elseif ($usertype === 's')
        {
            echo "<p>Welcome new babysitter, please enter your information below.</p>";
            
            $frmSitter = "<form id='frmSitter' name='frmSitter' method='POST' action='register.php'><table>";
            $frmSitter .= "<input type='hidden' name='type' id='type' value='s' />";
            $frmSitter .= "<tr><td><label for='name'>Name</label></td><td><input type='text' id='name' name='name' /></td></tr>";
            $frmSitter .= "<tr><td><label for='phone'>Phone</label></td><td><input type='text' id='phone' name='phone' /></td></tr>";
            $frmSitter .= "<tr><td><label for='email'>Email</label></td><td><input type='text' id='email' name='email' /></td></tr>";
            $frmSitter .= "<tr><td colspan='2'>Have you completed the registered babysitting training course?</td></tr>";
            $frmSitter .= "<tr><td></td><td><input type='radio' id='yes' name='trained' value='1' /><label for='yes'>Yes</label></td></tr>";
            $frmSitter .= "<tr><td></td><td><input type='radio' id='no' name='trained' value='0' checked='checked' /><label for='no'>No</label></td></tr>";
            $frmSitter .= "<tr><td colspan='2'>Please choose a username and password</td></tr>";
            $frmSitter .= "<tr><td><label for='uname'>Username</label></td><td><input type='text' id='uname' name='uname' /></td></tr>";
            $frmSitter .= "<tr><td><label for='pass'>Password</label></td><td><input type='password' id='pass' name='pass' /></td></tr>";
            $frmSitter .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Register' /></td></tr>";
            $frmSitter .= "</table></form>";
            echo $frmSitter;
        }
    }
    else //no type selected
    {
        echo "<p>Hi, welcome to the registration process. First off, are you a parent or a babysitter?</p>";
        echo "<div style='text-align: center; margin-bottom: 20px;'>";
        echo "<a href='register.php?type=p' class='bigbutton'><span class='bigbutton'>Parent</span></a>";
        echo "<a href='register.php?type=s' class='bigbutton'><span class='bigbutton'>Babysitter</span></a>";
        echo "</div>";
    }
}
else //form filled
{
    $usertype = $_REQUEST['type'];
    $uname = $_REQUEST['uname'];
    $pass = $_REQUEST['pass'];
    
    $newuser = new User($uname, $pass);
    if ($newuser->CheckDupUser() === false)
    {
        $name = $_REQUEST['name'];
        $phone = $_REQUEST['phone'];
        $email = $_REQUEST['email'];
        if ($usertype === 'p')
        {
            $newuser->FillParent($name, $phone, $email);
            $doreg = $newuser->RegisterParent();
            if ($doreg === true)
            {
                echo "<p>Registration succeeded! Now login to add some children.</p>";
                $frmParent = "<table>";
                $frmParent .= "<tr><td>Username</td><td>" . $newuser->uid . "</td></tr>";
                $frmParent .= "<tr><td>Name</td><td>" . $newuser->name . "</td></tr>";
                $frmParent .= "<tr><td>Phone</td><td>" . $newuser->phone . "</td></tr>";
                $frmParent .= "<tr><td>Email</td><td>" . $newuser->email . "</td></tr>";
                //$frmParent .= "<tr><td>Password</td><td>" . $newuser->pass . "</td></tr>";
                $frmParent .= "</table>";
                echo $frmParent;
            }
            else
            {
                echo $doreg;
            }
            //echo "<pre>";
            //$newUser->ToString();
            //echo "</pre>";
        }
        elseif ($usertype === 's')
        {
            $trained = $_REQUEST['trained'];
            $newuser->FillSitter($name, $phone, $email, $trained);
            $doreg = $newuser->RegisterSitter();
            if ($doreg === true)
            {
                echo "<p>Registration succeeded! </p>";
                $frmSitter = "<table>";
                $frmSitter .= "<tr><td>Username</td><td>" . $newuser->uid . "</td></tr>";
                $frmSitter .= "<tr><td>Name</td><td>" . $newuser->name . "</td></tr>";
                $frmSitter .= "<tr><td>Phone</td><td>" . $newuser->phone . "</td></tr>";
                $frmSitter .= "<tr><td>Email</td><td>" . $newuser->email . "</td></tr>";
                $frmSitter .= "<tr><td>Taken Training</td><td>" . ($newuser->trained == 1 ? "Yes" : "No") . "</td></tr>";
                //$frmSitter .= "<tr><td>Password</td><td>" . $newuser->pass . "</td></tr>";
                $frmSitter .= "</table>";
                echo $frmSitter;
            }
            else
            {
                echo $doreg;
            }
        }
    }
    else //dup user
    {
        echo "<p class='err'>Terribly sorry, but that username is already taken. Please try again.</p>";
    }
    
}

?>


<?php
    function __autoload($sClassName)
    {
        require_once("classes/$sClassName.class.php");        
    }
?>
<?php
    $sTitle = "Register";
    
    require_once("css/wrapper.php");
?>
<?php ob_start(); ?>

<?php
    $sitterid = "";
    if (isset($_REQUEST['id']))
    {
        $sitterid = $_REQUEST['id'];
    }
    if (isset($_COOKIE['parent']))
    {
        $user = unserialize($_COOKIE['parent']);
    }
    if (isset($user) && $user->usertype === 'p')
    {
        $db = new dbObject();
        $qrySitter = $db->selectQuery("*", "finSitters", "sitterID = '$sitterid'");
        $aSitter = $qrySitter->fetch_assoc();
        $sitter = new User($aSitter['sitterID'],$aSitter['password']);
        $sitter->FillSitter($aSitter['name'],$aSitter['phone'],$aSitter['email'], $aSitter['trained']);
        
        $sitter->LogSitterView($user->uid);
        
        echo "<h2>BABYSITTER - " . strtoupper($sitter->name) . "</h2>";
        
        $tblSitter = "<table>";
        $tblSitter .= "<tr><td><label for='name'>Name</label></td><td>" . $sitter->name . "</td></tr>";
        $tblSitter .= "<tr><td><label for='phone'>Phone</label></td><td>" . $sitter->phone . "</td></tr>";
        $tblSitter .= "<tr><td><label for='email'>Email</label></td><td>" . $sitter->email . "</td></tr>";
        $tblSitter .= "<tr><td>Taken Training</td><td>" . ($sitter->trained == 1 ? "Yes" : "No") . "</td></tr>";
        $tblSitter .= "</table>";
        echo $tblSitter;
    }
    else
    {
        setcookie("loginback", "sitter.php?id=$sitterid", time()+3600); //set cookie for loop back
        echo "<p class='err'>You must login as a parent in order to view this babysitter's information</p>";
    }
?>

<?php
    function __autoload($sClassName)
    {
        require_once("classes/$sClassName.class.php");        
    }
?>
<?php
    $sTitle = "Babysitter";
    if (isset($sitter))
    {
        $sTitle .= " - " . $sitter->name;
    }
    
    require_once("css/wrapper.php");
?>
<?php
	//gather all output from the output buffer and place it in a variable
	$sContent = ob_get_clean();
	//restart output
	ob_start();
?>
<!-- BEGIN THEME -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd"
    >
<html>
<head>
    <title>Community Babysitting - <?php echo $sTitle; ?></title>
    <link href="css/main.css" type="text/css" rel="stylesheet"/>
    
    <script type="text/javascript" src="js/jquery.js"> </script>
    <script type="text/javascript" src="js/jquery.validate.js"> </script>
    <script type="text/javascript" src="js/ValidRules.js"> </script>
</head>

<body>
    <div id='header'>
	<h1>Community Babysitting</h1>
        <div id='accounts'>
	    <?php
		$user = null;
		if (isset($_COOKIE['parent']))
		{
			$user = unserialize($_COOKIE['parent']);
		}
		elseif (isset($_COOKIE['sitter']))
		{
			$user = unserialize($_COOKIE['sitter']);
		}
		if ($user === null)
		{
			echo "[<a href='login.php'>Login</a>] [<a href='register.php'>Register</a>]";
		}
		else
		{
			echo "Welcome " . $user->name . " [<a href='settings.php'>Settings</a>] [<a href='login.php?logout=yes'>Logout</a>]";
		}
		
	    
	    ?>
        </div>
        <div id='menu'>
            <a href='babysitting.php'>Home</a> <a href='post.php'>Post Job</a> <a href='browse.php'>Browse Jobs</a>  
        </div>
    </div>
    
    <div id='main' >
        <?php
            echo $sContent;
        ?>
    </div>
    
    <div id='footer'>
	&copy; 2012 Kirk McCulloch
    </div>
    
</body>
</html>
<!-- END THEME -->
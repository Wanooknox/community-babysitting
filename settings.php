<?php ob_start(); ?>
<h2>USER SETTINGS</h2>

<?php
    if (isset($_COOKIE['parent']))
    {
        $user = unserialize($_COOKIE['parent']);
    }
    elseif (isset($_COOKIE['sitter']))
    {
        $user = unserialize($_COOKIE['sitter']);
    }
    if ($user->usertype === 'p')
    {
        if(isset($_REQUEST['parentid']))
        {
            $child = new Child("", $_REQUEST['name'], $_REQUEST['gender'], $_REQUEST['age'], $_REQUEST['parentid']);
            $goodadd = $child->AddChild();
            if($goodadd === true)
            {
                echo "<p>Child added!</p>";
            }
            else
            {
                echo $goodadd;
            }
        }
        if(isset($_REQUEST['uname']))
        {
            $user->FillParent($_REQUEST['name'], $_REQUEST['phone'], $_REQUEST['email']);
            $user->pass = $_REQUEST['pass'];
            $goodupdate = $user->UpdateParent();
            if($goodupdate === true)
            {
                echo "<p>Settings saved!</p>";
            }
            else
            {
                echo $goodupdate;
            }
        }
        $user->FetchChildren();
        
        $frmParent = "<form id='frmParent' name='frmParent' method='POST' action='settings.php'><table>";
        $frmParent .= "<input type='hidden' name='type' id='type' value='p' /> <input type='hidden' name='uname' id='uname' value='" . $user->uid . "' />";
        $frmParent .= "<tr><td>Username</td><td>" . $user->uid . "</td></tr>";
        $frmParent .= "<tr><td><label for='name'>Name</label></td><td><input type='text' id='name' name='name' value='" . $user->name . "'/></td></tr>";
        $frmParent .= "<tr><td><label for='phone'>Phone</label></td><td><input type='text' id='phone' name='phone' value='" . $user->phone . "'/></td></tr>";
        $frmParent .= "<tr><td><label for='email'>Email</label></td><td><input type='text' id='email' name='email' value='" . $user->email . "'/></td></tr>";
        $frmParent .= "<tr><td><label for='pass'>Password</label></td><td><input type='password' id='pass' name='pass' value='" . $user->pass . "'/></td></tr>";
        $frmParent .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Save' /></td></tr>";
        $frmParent .= "</table></form>";
        echo $frmParent;
        
        echo "<h3>CHILDREN</h3>";
        
        //$db = new dbObject();
        //$qryChildren = $db->selectQuery("*", "finChildren", "parentID = '" . $user->uid . "'");
        $children = $user->children;
        echo "<table border='1px'><tr><th>Name</th><th>Age</th><th>Gender</th></tr>";
        if (count($children) < 1)
        {
            echo "<tr><td colspan='3'>No Children</td></tr>";
        }
        foreach ($children as $key=>$val)
        {
            echo "<tr><td>" . $val->name . "</td><td>" . $val->age . "</td><td>" . $val->gender . "</td></tr>";
        }
        echo "</table><br />";
        
        $frmChild = "<form id='frmChild' name='frmChild' method='POST' action='settings.php'><table>";
        $frmChild .= "<tr><td colspan='2'><b>New Child</b> <input type='hidden' name='parentid' id='parentid' value='" . $user->uid . "' /></td></tr>";
        $frmChild .= "<tr><td><label for='name'>Name</label></td><td><input type='text' id='name' name='name' /></td></tr>";
        $frmChild .= "<tr><td><label for='age'>Age</label></td><td><input type='text' id='age' name='age' /></td></tr>";
        $frmChild .= "<tr><td><label for='gender'>Gender</label></td><td><select id='gender' name='gender' >";
        $frmChild .= "<option value='Male'>Male</option><option value='Female'>Female</option></select></td></tr>";
        $frmChild .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Add' /></td></tr>";
        $frmChild .= "</table></form>";
        echo $frmChild;
    }
    else
    {
        if(isset($_REQUEST['uname']))
        {
            $user->FillSitter($_REQUEST['name'], $_REQUEST['phone'], $_REQUEST['email'], $_REQUEST['trained']);
            $user->pass = $_REQUEST['pass'];
            $goodupdate = $user->UpdateSitter();
            if($goodupdate === true)
            {
                echo "<p>Settings saved!</p>";
            }
            else
            {
                echo $goodupdate;
            }
        }
        
        $frmSitter = "<form id='frmSitter' name='frmSitter' method='POST' action='settings.php'><table>";
        $frmSitter .= "<input type='hidden' name='type' id='type' value='s' /> <input type='hidden' name='uname' id='uname' value='" . $user->uid . "' />";
        $frmSitter .= "<tr><td>Username</td><td>" . $user->uid . "</td></tr>";
        $frmSitter .= "<tr><td><label for='name'>Name</label></td><td><input type='text' id='name' name='name' value='" . $user->name . "'/></td></tr>";
        $frmSitter .= "<tr><td><label for='phone'>Phone</label></td><td><input type='text' id='phone' name='phone' value='" . $user->phone . "'/></td></tr>";
        $frmSitter .= "<tr><td><label for='email'>Email</label></td><td><input type='text' id='email' name='email' value='" . $user->email . "'/></td></tr>";
        $frmSitter .= "<tr><td colspan='2'>Have you completed the registered babysitting training course?</td></tr>";
        $frmSitter .= "<tr><td></td><td><input type='radio' id='yes' name='trained' value='1' " . ($user->trained == 1 ? "checked='checked'" : "") ." /><label for='yes'>Yes</label></td></tr>";
        $frmSitter .= "<tr><td></td><td><input type='radio' id='no' name='trained' value='0' " . ($user->trained == 0 ? "checked='checked'" : "") ." /><label for='no'>No</label></td></tr>";
        $frmSitter .= "<tr><td><label for='pass'>Password</label></td><td><input type='password' id='pass' name='pass' value='" . $user->pass . "'/></td></tr>";
        $frmSitter .= "<tr><td colspan='2' style='text-align: right;'><input type='submit' value='Save' /></td></tr>";
        $frmSitter .= "</table></form>";
        echo $frmSitter;
    }

?>


<?php
    function __autoload($sClassName)
    {
        require_once("classes/$sClassName.class.php");        
    }
?>
<?php
    $sTitle = "Settings";
    
    require_once("css/wrapper.php");
?>
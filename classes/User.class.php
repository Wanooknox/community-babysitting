<?php
require_once("dbObject.class.php");
require_once("Child.class.php");

class User
{
	//any user info
	public $uid, $name, $phone, $pass, $email;
	//parent info
	public $children;
	//sitter info
	public $trained;
	//type flag
	public $usertype;
	
	
	function __construct($uid, $pass)
	{
		$db = new dbObject();
		$this->uid = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($uid)));
		$this->pass = $db->dbConnect->escape_string($pass);
	}
	
	/**
	* Function: FillParent
	* Purpose: If this user is a parent, call this function to insert the DB info
	* takes in a dbObject
	**/
	function FillParent($name, $phone, $email)
	{
		$db = new dbObject();
		$this->name = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($name)));
		$this->phone = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($phone)));
		$this->email = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($email)));
		$this->usertype = 'p';
		$this->FetchChildren();
	}
	
	/**
	* Function: FillParent
	* Purpose: If this user is a parent, call this function to insert the DB info
	* takes in a dbObject
	**/
	function FillSitter($name, $phone, $email, $trained)
	{
		$db = new dbObject();
		$this->name = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($name)));
		$this->phone = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($phone)));
		$this->email = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($email)));
		$this->trained = $trained;
		$this->usertype = 's';
	}
	
	function FetchChildren()
	{
		$dbChildren = new dbObject();
		$qryChildren = $dbChildren->selectQuery(
				"*",
				"finChildren",
				"parentID = '" . $this->uid . "'" );
		// echo "<pre>";
		// print_r($this->children);
		// echo "</pre>";
		$this->children = array();
		for($i = 0; $i < $qryChildren->num_rows; $i++)
		{
			$child = $qryChildren->fetch_assoc();
			
			$this->children[$i] = new Child($child["childID"], $child["name"], $child["gender"], $child["age"], $child["parentID"]);
		}
		// echo "<pre>";
		// print_r($this->children);
		// echo "</pre>";
		return $this->children;
	}
	
	/**
	* Function: RegisterParent
	* Purpose: If this user is a new user, call this function to add them to the database
	* takes in a dbObject
	**/
	function RegisterParent()
	{
		$result = $this->ValidateInfo();
		
		if ($result === false) //no invalid data found
		{
			$db = new dbObject();
			$qryReg = $db->insertQuery(
				"finParents",
				"parentID, name, phone, email, password",
				"'" . $this->uid . "', '" . $this->name . "', '" . $this->phone . "', '" . $this->email . "', '" . $this->pass . "'");
			$result = $qryReg;
		}
		
		return $result;
	}
	
	/**
	* Function: RegisterSitter
	* Purpose: If this user is a new user, call this function to add them to the database
	* takes in a dbObject
	**/
	function RegisterSitter()
	{
		$result = $this->ValidateInfo();
		
		if ($result === false)
		{
			$db = new dbObject();
			$qryReg = $db->insertQuery(
				"finSitters",
				"sitterID, name, phone, email, trained, password",
				"'" . $this->uid . "', '" . $this->name . "', '" . $this->phone . "', '" . $this->email . "', '" . $this->trained . "', '" . $this->pass . "'");
			$result = $qryReg;
		}
		
		return $result;
	}
	
	//takes current object values and commits them back to the DB
	function UpdateParent()
	{
		$result = $this->ValidateInfo();
		if($result === false)
		{
			$db = new dbObject();
			$aValues = array("parentID"=>$this->uid, "name"=>$this->name, "phone"=>$this->phone,"email"=>$this->email,"password"=>$this->pass);
			$qryUpdate = $db->updateQuery($aValues, "finParents", "parentID");
			$this->FreshCookie();
			$result = $qryUpdate;
		}
		return $result;
	}
	
	//takes current object values and commits them back to the DB
	function UpdateSitter()
	{
		$result = $this->ValidateInfo();
		if($result === false)
		{
			$db = new dbObject();
			$aValues = array("sitterID"=>$this->uid, "name"=>$this->name,
					 "phone"=>$this->phone,"email"=>$this->email,"trained"=>$this->trained,"password"=>$this->pass);
			$qryUpdate = $db->updateQuery($aValues, "finSitters", "sitterID");
			$this->FreshCookie();
			$result = $qryUpdate;
		}
		return $result;
	}
	
	function LogSitterView($parentID)
	{
		$db = new dbObject();
		$qryLog = $db->insertQuery("finSitterViews", "viewID, sitterID, parentID, date, time",
					   "NULL, '" . $this->uid . "', '$parentID', CURDATE(), CURTIME()");
	}
	
	function ValidateInfo()
	{
		$result = "";
		
		if (preg_match("/^[A-Za-z \-']+$/", $this->name) == 0)
		{
			$result .= "<p class='err'>Invalid name was entered. Can only contain letters, dashes, spaces, and apostrophes.</p>";
		}
		if (preg_match("/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/", $this->phone) == 0)
		{
			$result .= "<p class='err'>Invalid phone number was entered. Use the form (123) 456-7890 or 123-4567.</p>";
		}
		if (preg_match("/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/", $this->email) == 0)
		{
			$result .= "<p class='err'>Invalid email was entered. Can only contain letters, numbers, dots, underscores, and dashes. Use the form john_doe@example.com.</p>";
		}
		if ($result === "")
		{
			$result = false;
		}
		return $result;
	}
	
	function CheckDupUser()
	{
		$result = false;
		$db = new dbObject();
		$qryParent = $db->selectQuery(
				"parentID",
				"finParents",
				"parentID = '" . $this->uid . "'" );
		$qrySitter = $db->selectQuery(
				"sitterID",
				"finSitters",
				"sitterID = '" . $this->uid . "'" );
		if ($qryParent->num_rows > 0 || $qrySitter->num_rows > 0)
		{
			$result = true;
		}
		return $result;
	}
	
	function CheckValidCredentials()
	{
		$result = false;
		$db = new dbObject();
		$qryParent = $db->selectQuery(
				"parentID",
				"finParents",
				"parentID = '" . $this->uid . "' AND password = '" . $this->pass . "'" );
		$qrySitter = $db->selectQuery(
				"sitterID",
				"finSitters",
				"sitterID = '" . $this->uid . "' AND password = '" . $this->pass . "'" );
		if ($qryParent->num_rows > 0 )
		{
			$result = 'p';
		}
		elseif($qrySitter->num_rows > 0)
		{
			$result = 's';
		}
		return $result;
	}
	
	/**
	* Function: Login
	* Purpose: log this user object in
	**/
	function Login()
	{
		$result = false;
		$isvalid = $this->CheckValidCredentials();
		$db = new dbObject();
		
		if ($isvalid === 'p')
		{
			$qryParent = $db->selectQuery(
				"*",
				"finParents",
				"parentID = '" . $this->uid . "' AND password = '" . $this->pass . "'" );
			$aInfo = $qryParent->fetch_assoc();
			$this->FillParent($aInfo['name'], $aInfo['phone'], $aInfo['email']);
			setcookie("parent", serialize($this), time()+3600*24*365);
			$result = true;
		}
		elseif($isvalid === 's')
		{
			$qryParent = $db->selectQuery(
				"*",
				"finSitters",
				"sitterID = '" . $this->uid . "' AND password = '" . $this->pass . "'" );
			$aInfo = $qryParent->fetch_assoc();
			$this->FillSitter($aInfo['name'], $aInfo['phone'], $aInfo['email'], $aInfo['trained']);
			setcookie("sitter", serialize($this), time()+3600*24*365);
			$result = true;
		}
		else
		{
			$result = "<p class='err'>Invalid username or password.</p>";	
		}
		//$_SESSION['user'] = serialize($this);
		return $result;
	}
	
	/**
	* Function: Logout
	* Purpose: log this user object out
	**/
	function Logout()
	{
		setcookie("parent", "", 0);
		setcookie("sitter", "", 0);
		unset($_COOKIE['parent'], $_COOKIE['sitter']);
	}
	
	function FreshCookie()
	{
		if(isset($_COOKIE['parent']))
		{
			setcookie("parent", serialize($this), time()+3600*24*365);
			$_COOKIE["parent"] = serialize($this);
		}
		if(isset($_COOKIE['sitter']))
		{
			setcookie("sitter", serialize($this), time()+3600*24*365);
			$_COOKIE["sitter"] = serialize($this);
		}
	}
	
	function __destruct()
	{
		//$this->FreshCookie();
	}
}

?>

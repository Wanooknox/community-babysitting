<?php
require_once("dbObject.class.php");

class Child
{
    public $cid, $name, $gender, $age, $parentid;
    
    function __construct($cid, $name, $gender, $age, $parentid)
    {
        $db = new dbObject();
        $this->cid = $db->dbConnect->escape_string($cid);
        $this->name = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($name)));
        $this->gender = $db->dbConnect->escape_string($gender);
        $this->age = $db->dbConnect->escape_string(htmlspecialchars(strip_tags($age)));
        $this->parentid = $db->dbConnect->escape_string($parentid);
    }
    
    function AddChild()
    {
        $result = "";
        if (preg_match("/^[A-Za-z \-']+$/", $this->name) == 0)
        {
            $result .= "<p class='err'>Invalid name was entered. Can only contain letters, dashes, and apostrophes.</p>";
        }
        if (preg_match("/^\d+$/", $this->age) == 0)
        {
            $result .= "<p class='err'>Invalid age was entered. Must be a numeric value.</p>";
        }
        if($result === "")
        {
            $result = false;
        }
        if($result === false)
        {
            $db = new dbObject();
            $qryChild = $db->insertQuery("finChildren", "childID, name, gender, age, parentID",
                                         "NULL, '" . $this->name . "', '" . $this->gender . "', '" . $this->age . "', '" . $this->parentid . "'");
            $result = $qryChild;
        }
        return $result;
    }
}

?>
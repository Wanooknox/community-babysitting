<?php
require_once("dbObject.class.php");

class Job
{
    public $jid, $parentid, $day, $start, $end;
    
    function __construct($jid, $parentid, $day, $start, $end)
    {
        $this->jid = $jid;
        $this->parentid = $parentid;
        $this->day = $day;
        $this->start = $start;
        $this->end = $end;
    }
    
    function PostJob()
    {
        $db = new dbObject();
        $qryJob = $db->insertQuery("finJobs", "jobID, parentID, date, starttime, endtime",
                                   "NULL, '" . $this->parentid . "', '" . $this->day . "', '" . $this->start . "', '" . $this->end . "'");
        return $qryJob;
    }
	
	function PrintSearchJob()
	{
	    echo "<a id='" . $this->jid . "' class='jobli'>" . @date("g:i a", strtotime($this->start)) . " - " . @date("g:i a", strtotime($this->end)) . "</a>";
	}
	
	function PrintJobDetails()
	{
            $db = new dbObject();
            $qryParent = $db->selectQuery("*", "finParents", "parentID = '" . $this->parentid . "'");
            $aParent = $qryParent->fetch_assoc();
            $rent = new User($aParent['parentID'],$aParent['password']);
            $rent->FillParent($aParent['name'],$aParent['phone'],$aParent['email']);
            
            echo "<h3>" . $rent->name . "</h3>";
            $children = $rent->children;
            $height = count($children) + 1;
            echo "<table cellspacing='0'><tr><th>Name</th><th>Age</th><th>Gender</th>";
            echo "<td valign='top' rowspan='$height'>&nbsp;&nbsp;&nbsp;&nbsp;Start: " . @date("g:i a", strtotime($this->start));
            echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;End: " . @date("g:i a", strtotime($this->end)) . "</td></tr>";
            foreach ($children as $key=>$val)
            {
                echo "<tr><td style='border: 1px solid black;'>" . $val->name
                    . "</td><td style='border: 1px solid black;'>" . $val->age
                    . "</td><td style='border: 1px solid black;'>" . $val->gender . "</td></tr>";
            }
            echo "</table><br />";
		
		echo "<span id='applied'><input type='button' id='" . $this->jid . "' class='jobapp' value='Apply'/></span>";
	}
	/**
	* Function: EmailAdmin
	* Purpose: email the administrators when new users register
	**/
	function ApplyForJob($sitter)
	{
		$db = new dbObject();
		//select all the administration users from the database
		$qrySitter = $db->selectQuery(
				"email, name",
				"finSitters",
				"sitterID = '$sitter'" );
		$aSitter = $qrySitter->fetch_assoc();
				
		$qryParent = $db->selectQuery("email, name", "finParents", "parentID = '" . $this->parentid . "'");
		$aParent = $qryParent->fetch_assoc();
		
		$subject = "Babysitter has applied for a job";
		$to = $aParent['email'];
		$message = "<h1>Babysitter Application</h1><p>A new babysitter has applied for a job. ";
		$message .= "Click the link below to see their information</p>";
		$message .= "<p><a href='http://kelcstu05/~cst224/Final/sitter.php?id=$sitter'>" . $aSitter['name'] . "</a>";
		$message .= " or copy and paste: http://kelcstu05/~cst224/Final/sitter.php?id=$sitter</p>";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Application Daemon <noreply@cstsitterservice.com>';
		mail($to, $subject, $message, $headers);
	}
}

?>
<?php
    class cstForm
    {
        //String attribute for building up a form
        protected $sForm;
        /*******************************************************
         *Function: Constructor
         *Purpose:  This starts the process by which we are going
         *          to build up our text representation of our
         *          form object.  Remember that forms have a number
         *          of attributes associated with them that should be
         *          set -- these shall be passed in.
         *******************************************************/
        public function __construct($sName, $sAction, $sMethod="GET")
        {
            $this->sForm = "<form name='$sName' id='$sName' method='$sMethod' action='$sAction'>";
        }
        
        /********************************************************
         *Function: endForm
         *Purpose:  This routine completes our form by outputting
         *          a submit button and a reset button
         *Params:   sSubmit, sReset
         ********************************************************/
        public function endForm($sSubmit="Submit Form", $sReset="Reset")
        {
            $this->sForm .= "<br/><input type='submit' value='$sSubmit' />" .
                "<input type='reset' value='$sReset' /></form>";
        }
        
        /*********************************************************
         *Function: toString
         *Purpose:  With this method we assume that the programmer
         *          has finished with the form.  Return the string
         *          we are building.
         *********************************************************/
        public function toString()
        {
            return $this->sForm;
        }
        
        /*********************************************************
         *Function: addTextBox
         *Purpose:  This routine will just add a label and a textbox
         *          to our form.  The label and the textbox names must
         *          be passed in.
         *Params:   $sLabel, $sTxtName, $sOpts
         *********************************************************/
        public function addTextBox($sLabel, $sTxtName, $sOpts="")
        {
            $this->sForm .= "<br/>$sLabel <input type='text' name='$sTxtName' id='$sTxtName' $sOpts />\n";
        }
        
        /*********************************************************
         *Function: addHiddenTextBox
         *Purpose:  This routine will just add a hidden textbox to
         *          our form. The text box name and value must be passed in.
         *Params:   sTxtName - name of the hidden textbox
         *          sValue - the value to be return for this textbox
         *          sOpts - additional options
         ********************************************************/
        public function addHiddenTextBox($sTxtName, $sValue, $sOpts="")
        {
            $this->sForm .= "<br/><input type='hidden' name='$sTxtName' id='$sTxtName' $sOpts />\n";
        }
        
        /*********************************************************
         *Function: addTextGroup
         *Purpose:  This routine will add to our form string, values
         *          taken from an associative array of textboxes and
         *          their associated label values.
         *Params:   $aTxtList, $sOpts
         *********************************************************/
        public function addTextGroup($aTxtList, $sOpts="")
        {
            foreach($aTxtList as $sTxtName=>$sLabel)
            {
                $this->addTextBox($sLabel, $sTxtName, $sOpts);
            }
        }
        
        /*********************************************************
         *Function: addRadioButton
         *Purpose:  This routine will just add a label and a radio button
         *          to our form.  
         *Params:   $sLabel, $sRadName, $sValue, $sOpts
         *********************************************************/
        public function addRadioButton($sLabel, $sGrpName, $sRdoID, $sValue, $sOpts="")
        {
            $this->sForm .= "<br/>$sLabel <input type='radio' name='$sGrpName' value='$sValue' id='$sRdoID' />\n";
        }
        
        /*********************************************************
         *Function: addRadioGroup
         *Purpose:  Someone wants to have a radio button group.
         *Params:   $aRadVals, $sName, $sOpts
         *********************************************************/
        public function addRadioGroup($aRadVals, $sName, $sOpts="")
        {
            foreach($aRadVals as $sValue=>$sLabel)
            {
                $this->addRadioButton($sLabel, $sName, $sValue,  $sOpts);
            }         
        }
        
        /*********************************************************
         *Function: addHeader
         *Purpose:  Adds a header to the output screen.
         *Params:   $sHeaderText
         *********************************************************/
        public function addHeader($sHeaderText)
        {
            $this->sForm .= "<h3>$sHeaderText</h3>";
        }
        
        /*********************************************************
         *Function: addSelect
         *Purpose:  This routine will add a select box of some type
         *          to our form.
         *Params:   $sName, $sText, $aOptions, $sOpts 
         *********************************************************/
        public function addSelect($sName, $sText, $aOptions, $sOpts="")
        {
            $this->sForm .= "<br/>$sText<select name='$sName' id='$sName' $sOpts>\n";
            $this->addSelectOptions($aOptions);
            $this->sForm .= "</select>";
        }
        
        /*********************************************************
         *Function: addSelectOptions
         *Purpose:  This routine will add the options for our select box.
         *Params:   $aOptions
         *********************************************************/
        protected function addSelectOptions($aOptions)
        {
            foreach($aOptions as $sValue=>$sLabel)
            {
                $this->sForm .= "<option value='$sValue' >$sLabel</option>";
            } 
        }
        
        
        
        
        
        
        
    }
?>
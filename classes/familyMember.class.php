<?php
    class familyMember
    {
        public $member;
        
        function __construct()
        {
                    
        }
        
        public function getCurrFamilyMember($fam_ID)
        {            
            $dbFamilyTree = new dbObject();
            
            $sColumnList = "*";
            $sTableList = "ftFamilyMembers";
            $sCondition = "fam_ID = $fam_ID";
                                
            $qryResults = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
    
            $nRows = $qryResults->num_rows;
                            
            for ($i = 0; $i < $nRows; $i++)
            {                    
                $aRow = $qryResults->fetch_assoc();
                foreach($aRow as $key=>$sFieldValue)
                {                        
                    $aRowVals[$key] = htmlspecialchars($sFieldValue);
                }
            }
            
            return $aRowVals;
        }
        
        public function getMotherRelation($fam_ID)
        {
            $dbFamilyTree = new dbObject();
            
            $sColumnList = "*";
            $sTableList = "ftFamilyRelationships";
            $sCondition = "fam_ID = $fam_ID AND relationship = 'Mother'";
            
            $qryMotherRelation = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
            
            $MotherRelation = "";
                      
            $aMomRow = $qryMotherRelation->fetch_assoc();
            if ($aMomRow != null)
            {
                foreach($aMomRow as $key=>$sFieldValue)
                {                        
                    $MotherRelation[$key] = htmlspecialchars($sFieldValue);
                }
            }
            
            return $MotherRelation;
        }
        
        public function getFatherRelation($fam_ID)
        {
            $dbFamilyTree = new dbObject();
            
            $sColumnList = "*";
            $sTableList = "ftFamilyRelationships";
            $sCondition = "fam_ID = $fam_ID AND relationship = 'Father'";
            
            $qryFatherRelation = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
            
            $FatherRelation = "";
                      
            $aDadRow = $qryFatherRelation->fetch_assoc();
            if ($aDadRow != null)
            {
                foreach($aDadRow as $key=>$sFieldValue)
                {                        
                    $FatherRelation[$key] = htmlspecialchars($sFieldValue);
                }
            }
            
            return $FatherRelation;
        }
        
        public function getSpouseRelation($fam_ID)
        {
            $dbFamilyTree = new dbObject();
            
            $sColumnList = "*";
            $sTableList = "ftFamilyRelationships";
            $sCondition = "fam_ID = $fam_ID AND relationship = 'Spouse'";
            
            $qrySpouseRelation = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
            
            $SpouseRelation = "";
                      
            $aSpouseRow = $qrySpouseRelation->fetch_assoc();
            if ($aSpouseRow != null)
            {
                foreach($aSpouseRow as $key=>$sFieldValue)
                {                        
                    $SpouseRelation[$key] = htmlspecialchars($sFieldValue);
                }
            }
            
            return $SpouseRelation;
        }
        
        public function getChildRelation($qryChildRelation, $nChildRows)
        {
            for ($i = 0; $i < $nChildRows; $i++)
            {  
                $aChildRow = $qryChildRelation->fetch_assoc();		
                $x = 0;
                foreach($aChildRow as $sFieldValue)
                {                        
                    $ChildRelation[$x] = htmlspecialchars($sFieldValue);
                    $x++;
                }
                $childrenID[$i] = $ChildRelation[2];
            }
            
            return $childrenID;
        }
        
        public function getMothersName($relatedFamily_ID, $MotherRelation)
        {
            $dbFamilyTree = new dbObject();
            
            //query the database to get the mother's name
            $sColumnList = "*";
            $sTableList = "ftFamilyMembers";
            $sCondition = "fam_ID = $MotherRelation[relatedFamily_ID]";
            
            $qryMomName = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
      
            $aMom = $qryMomName->fetch_assoc();
            $x = 0;
            foreach($aMom as $sFieldValue)
            {                        
                $MomName[$x] = htmlspecialchars($sFieldValue);
                $x++;
            }
            print "<td><a href='familyMember.php?id=$MotherRelation[relatedFamily_ID]'>$MomName[1] $MomName[2]</a>";
        }
        
        public function getFathersName($relatedFamily_ID, $FatherRelation)
        {
            $dbFamilyTree = new dbObject();
            
            //query the database to get the mother's name
            $sColumnList = "*";
            $sTableList = "ftFamilyMembers";
            $sCondition = "fam_ID = $FatherRelation[relatedFamily_ID]";
            
            $qryDadName = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
      
            $aDad = $qryDadName->fetch_assoc();
            $x = 0;
            foreach($aDad as $sFieldValue)
            {                        
                $DadName[$x] = htmlspecialchars($sFieldValue);
                $x++;
            }
            print "<td><a href='familyMember.php?id=$FatherRelation[relatedFamily_ID]'>$DadName[1] $DadName[2]</a>";
        }
        
        public function getSpousesName($relatedFamily_ID, $SpouseRelation)
        {
            $dbFamilyTree = new dbObject();
            
            //query the database to get the father's name
            $sColumnList = "*";
            $sTableList = "ftFamilyMembers";
            $sCondition = "fam_ID = $SpouseRelation[relatedFamily_ID]";
            
            $qrySpouseName = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
      
            $aSpouse = $qrySpouseName->fetch_assoc();
            $x = 0;
            foreach($aSpouse as $sFieldValue)
            {                        
                $SpouseName[$x] = htmlspecialchars($sFieldValue);
                $x++;
            }
            print "<td><a href='familyMember.php?id=$SpouseRelation[relatedFamily_ID]'>$SpouseName[1] $SpouseName[2]</a>";
        }
        
        public function removeSpouseRelation($fam_ID)
        {
            $dbFamilyTree = new dbObject();
            
            //query database for the spouse of the fam_ID just passed in
            //$sColumnList = "relatedFamily_ID";
            //$sTableList = "ftFamilyRelationships";
            //$sCondition = "fam_ID = $fam_ID AND relationship='Spouse'";
            //
            //$qrySpouse = $dbFamilyTree->selectQuery($sColumnList, $sTableList, $sCondition);
            //
            //$aSpouse = $qrySpouse->fetch_assoc();
            //$relatedFamily_ID = htmlspecialchars($aSpouse['relatedFamily_ID']);
            
            //delete relationship
            $sTableList = "ftFamilyRelationships";
            $sCondition = "fam_ID = $fam_ID AND relationship='Spouse'";
            $dbFamilyTree->deleteQuery($sTableList, $sCondition);
            $sCondition = "relatedFamily_ID = $fam_ID AND relationship='Spouse'";
            $dbFamilyTree->deleteQuery($sTableList, $sCondition);
        }
    }
?>
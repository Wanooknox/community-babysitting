<?php
require_once("cstForm.class.php");

class cstTabForm extends cstForm
{
    //This class is designed to output forms in a tabular fashion
    //we will be working with an Nx2 table output (two columns)
    
    /***************************
     *Function: Constructor
     *Purpose: start the process of building our tabl form
     *Params: sFrmName - name of the form
     *        sFrmAction - where the submitted form will go.
     *        sFrmMethod - how it will get there
     *        sTabOpts - aditional options
     ************************/
    public function __construct($sFrmName, $sFrmAction, $sFrmMethod="GET", $sTabOpts="")
    {
        //call the parten class constructor
        parent::__construct($sFrmName, $sFrmAction, $sFrmMethod);
        
        $this->sForm .= "<table $sTabOpts>\n";
    }
    
    /************************
     *Function: endForm()
     *Purpose: finish off the elements for the string that is to represent
     *our form tab output.
     *Params: sSubmit - any text that you want on the submit button
     *        sReset - any text that you want on the reset button
     *************************/
    public function endForm($sSubmit="Submit", $sReset="Clear")
    {
        $this->addTableRow("&nbsp;","<input type='submit' value='$sSubmit' /> <input type='reset' value='$sReset' />");
        $this->sForm .= "</table></form>\n";
    }
    
    
    public function endAdminForm($sSubmit="Submit Form")
    {
        $this->addTableRow("&nbsp;","<input type='submit' value='$sSubmit' />");
        $this->sForm .= "</table></form>\n";
    }
    
    
    public function addTableRow($sLabel, $sValue)
    {
        $this->sForm .= "<tr><td>$sLabel</td><td>$sValue</td></tr>\n";
    }
    
    public function addPassBox($sLabel, $sName, $sOpts="")
    {
        $this->addTableRow($sLabel, "<input type='password' name='$sName' id='$sName' $sOpts />");
    }
    
    public function addTextBox($sLabel, $sTxtName, $sOpts="")
    {
        $this->addTableRow($sLabel, "<input type='text' name='$sTxtName' id='$sTxtName' $sOpts />");
    }
    
    /*********************************************************
    *Function: addHiddenTextBox
    *Purpose:  This routine will just add a hidden textbox to
    *          our form. The text box name and value must be passed in.
    *Params:   sTxtName - name of the hidden textbox
    *          sValue - the value to be return for this textbox
    *          sOpts - additional options
    ********************************************************/
    public function addHiddenTextBox($sTxtName, $sValue, $sOpts="")
    {
        $this->addTableRow("", "<input type='hidden' name='$sTxtName' id='$sTxtName' value='$sValue' />\n");
    }
    
    /*************************************************************
     *Function: addCheckBox
     *Purpose:  Add a check box field to our form
     *Params:   sName - name of the check box
     *          sLabel - label for the check box
     *          sValue - value submitted if checkbox is checked
     *          bInitialState - true if check box is to be checked by default
     ************************************************************/
    public function addCheckBox($sName, $sLabel, $sValue, $bInitialState=false)
    {
        $sCheckBox = "<input type='checkbox' name='$sName' id='$sName' " .
                    "value='$sValue'";
                    
        if ($bInitialState)
        {
            $sCheckBox .= " checked='checked'";
        }
        
        $sCheckBox .= " />";
        $this->addTableRow($sLabel, $sCheckBox);
    }
    
    public function addRadioButton($sLabel = "", $sGrpName, $sRdoID, $sValue, $sOpts="")
    {
        $this->addTableRow("", "<input type='radio' name='$sGrpName' id='$sRdoID' value='$sValue' $sOpts />" .
            "<label for='$sRdoID'>$sLabel</label>");
    }
    
    public function addHeader($sHeaderText)
    {
        $this->sForm .= "<tr><td colspan='2' style='text-align:center'><h3>$sHeaderText</h3></td></tr>\n";
    }
    
    public function addSelect($sLabel, $sName, $aOptList, $sOpts="")
    {
        $this->sForm .= "<tr><td><label for='$sName'>$sLabel</label> </td>";
        $this->sForm .= "<td><select id='$sName' name='$sName' $sOpts>\n";
        foreach($aOptList as $value=>$text)
        {
            $this->sForm .= "<option value='$value'>$text</option>\n";
        }
        $this->sForm .= "</select></td></tr>\n";
    }
    
    public function addTextArea($sName, $sLabel, $nRows=3, $nCols=40, $sText="" )
    {
        $this->addTableRow($sLabel, "<textarea name='$sName' id='$sName' rows='$nRows' cols='$nCols' >$sText</textarea>");
    }
    
}

?>

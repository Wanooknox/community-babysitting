<?php
    class addFamily
    {
        function __construct()
        {
                    
        }
        
        public function getFamilyMemberJustAdded($first_name, $last_name)
        {
            $dbFamilyTree = new dbObject();
            
            //get the user id of the family member that was just added
            $sColumnList = "fam_ID";
            $sTable = "ftFamilyMembers";
            $sCondition = "first_name = '$first_name' AND last_name = '$last_name'";
            
            $qryRelatedFamily_ID = $dbFamilyTree->selectQuery($sColumnList, $sTable, $sCondition);
            $nRows = $qryRelatedFamily_ID->num_rows;
                        
	    for ($i = 0; $i < $nRows; $i++)
	    {                    
		$aRow = $qryRelatedFamily_ID->fetch_assoc();
		$x = 0;
		foreach($aRow as $sFieldValue)
		{                        
		    $relatedFamily_ID = htmlspecialchars($sFieldValue);
		}
	    }
            
            return $relatedFamily_ID;
        }
        
        public function addMotherFatherRelation($relatedFamily_ID, $fam_ID, $relationship, $sColumnList, $sTable)
        {
            $dbFamilyTree = new dbObject();
            
            $sValues = "$fam_ID, $relatedFamily_ID, '" . $relationship . "'";
            $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            $sValues = "$relatedFamily_ID, $fam_ID, 'Child'";
            $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            
            if ($relationship == 'Mother')
            {
                $oppositeParent = 'Father';
            }
            else
            {
                $oppositeParent = 'Mother';
            }
            
            //check to see if the "fam_ID" user has a relationship opposite to the one of the parent added
            $sColumnList = "relatedFamily_ID";
            $sTable = "ftFamilyRelationships";
            $sCondition = "fam_ID = " . $fam_ID . " AND relationship = '" . $oppositeParent . "'";
            $qryHasAnotherParent = $dbFamilyTree->selectQuery($sColumnList, $sTable, $sCondition);
            
            if ($qryHasAnotherParent->num_rows > 0)
            {
                $aRow = $qryHasAnotherParent->fetch_assoc();
                foreach($aRow as $sFieldValue)
                {                        
                    $oppositeParent_ID = htmlspecialchars($sFieldValue);
                }
                
                //check if the opposite parent has more than one child
                $sColumnList = "relatedFamily_ID";
                $sTable = "ftFamilyRelationships";
                $sCondition = "fam_ID = " . $oppositeParent_ID . " AND relatedFamily_ID <> " . $fam_ID . " AND relationship = 'Child'";
                $qryNumChild = $dbFamilyTree->selectQuery($sColumnList, $sTable, $sCondition);
                $numChildren = $qryNumChild->num_rows;
                if ($numChildren > 0)
                {
                    for ($i = 0; $i < $numChildren; $i++)
                    {                    
                        $aRow = $qryNumChild->fetch_assoc();
                        $childID = htmlspecialchars($aRow['relatedFamily_ID']);
                        
                        print $relatedFamily_ID[$i];
                        $sColumnList = "fam_ID, relatedFamily_ID, relationship";
                        $sTable = "ftFamilyRelationships";
                        $sValues = "$relatedFamily_ID, $childID, 'Child'";
                        $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
                        $sValues = "$childID, $relatedFamily_ID, '" . $relationship . "'";
                        $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
                    }                    
                }
                
                $sColumnList = "fam_ID, relatedFamily_ID, relationship";
                $sTable = "ftFamilyRelationships";
                $sValues = "$relatedFamily_ID, $oppositeParent_ID, 'Spouse'";
                $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
                $sValues = "$oppositeParent_ID, $relatedFamily_ID, 'Spouse'";
                $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            }
            
            //add relationship to the opposite parent for each child that the current parent has
        }
        
        public function addChildParentRelation($relatedFamily_ID, $fam_ID, $relationship, $sColumnList, $sTable)
        {
            $dbFamilyTree = new dbObject();
            
            $sValues = "$fam_ID, $relatedFamily_ID, 'Child'";
            $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            
            if ($_REQUEST['txtRelationToChild'] == 'Female')
            {
                $sValues = "$relatedFamily_ID, $fam_ID, 'Mother'";
                $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            }
            else
            {
                $sValues = "$relatedFamily_ID, $fam_ID, 'Father'";
                $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            }
            
            $sColumnList = "relatedFamily_ID";
            $sTable = "ftFamilyRelationships";
            $sCondition = "fam_ID = " . $fam_ID . " AND relationship = 'Spouse'";
            $qryParentHasSpouse = $dbFamilyTree->selectQuery($sColumnList, $sTable, $sCondition);
            
            if ($qryParentHasSpouse->num_rows > 0)
            {
                $aRow = $qryParentHasSpouse->fetch_assoc();
                foreach($aRow as $sFieldValue)
                {                        
                    $Parent_ID = htmlspecialchars($sFieldValue);
                }  
                
                $sColumnList = "fam_ID, relatedFamily_ID, relationship";
                $sTable = "ftFamilyRelationships";
                $sValues = "$Parent_ID, $relatedFamily_ID, 'Child'";
                $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
                
                //find parent's gender
                $sColumn = "gender";
                $sTableList = "ftFamilyMembers";
                $sCondition = "fam_ID = " . $Parent_ID;
                $qryParentGender = $dbFamilyTree->selectQuery($sColumn, $sTableList, $sCondition);
                
                $aRow = $qryParentGender->fetch_assoc();
                foreach($aRow as $sFieldValue)
                {                        
                    $gender = htmlspecialchars($sFieldValue);
                } 
                
                if ($gender == 'Female')
                {
                    $Parent = 'Mother';
                }
                else
                {
                    $Parent = 'Father';
                }		    
                
                $sValues = "$relatedFamily_ID, $Parent_ID, '" . $Parent . "'";
                $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            }
        }
        
        public function addSpouseRelation($relatedFamily_ID, $fam_ID, $relationship, $sColumnList, $sTable)
        {
            $dbFamilyTree = new dbObject();
            
            $sValues = "$fam_ID, $relatedFamily_ID, 'Spouse'";
            $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            $sValues = "$relatedFamily_ID, $fam_ID, 'Spouse'";
            $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
            
            //check to see if the "fam_ID" user has a any children
            $sColumnList = "relatedFamily_ID";
            $sTable = "ftFamilyRelationships";
            $sCondition = "fam_ID = " . $fam_ID . " AND relationship = 'Child'";
            $qryHasChildren = $dbFamilyTree->selectQuery($sColumnList, $sTable, $sCondition);
            $nRows = $qryHasChildren->num_rows;
            if ($qryHasChildren->num_rows > 0)
            {
                for ($i = 0; $i < $nRows; $i++)
                {
                    $aRow = $qryHasChildren->fetch_assoc();
                    foreach($aRow as $sFieldValue)
                    {                        
                        $children_ID[$i] = htmlspecialchars($sFieldValue);
                    }
                    
                    $sColumnList = "fam_ID, relatedFamily_ID, relationship";
                    $sTable = "ftFamilyRelationships";
                    $sValues = "$relatedFamily_ID, $children_ID[$i], 'Child'";
                    $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
                    
                    //find parent's gender
                    $sColumn = "gender";
                    $sTableList = "ftFamilyMembers";
                    $sCondition = "fam_ID = " . $relatedFamily_ID;
                    $qryParentGender = $dbFamilyTree->selectQuery($sColumn, $sTableList, $sCondition);
                    
                    $aRow = $qryParentGender->fetch_assoc();
                    foreach($aRow as $sFieldValue)
                    {                        
                        $gender = htmlspecialchars($sFieldValue);
                    } 
                    
                    if ($gender == 'Female')
                    {
                        $Parent = 'Mother';
                    }
                    else
                    {
                        $Parent = 'Father';
                    }
                    
                    $sValues = "$children_ID[$i], $relatedFamily_ID, '" . $Parent . "'";
                    $dbFamilyTree->insertQuery($sTable, $sColumnList, $sValues);
                }    
            }
        }
    }
?>
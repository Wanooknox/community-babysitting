<?php ob_start(); ?>
<h2>BROWSE JOBS</h2>
<?php
	if (isset($_COOKIE['sitter']))
    {
        $user = unserialize($_COOKIE['sitter']);
    }
	if (isset($user) && $user->usertype === 's')
	{
?>

<script type="text/javascript">
   
    var ajaxDate = function(date) {
        //do an ajax call to a php functin that returns html of the search results
        //on success out out the results
        $.ajax(
        {
            url: "ajax/ajaxDate.php?date=" + date,
            type: "get",
            success: outputJobList,
            error: function(obErr,obStat,txtMsg) {alert(obErr)}
        });
    };
	
	var ajaxDetails = function(id) {
        //do an ajax call to a php functin that returns html of the search results
        //on success out out the results
        $.ajax(
        {
            url: "ajax/ajaxDetails.php?id=" + id,
            type: "get",
            success: outputJobDetails,
            error: function(obErr,obStat,txtMsg) {alert(obErr)}
        });
		
		
    };
    
	var ajaxApply = function(id) {
        //do an ajax call to a php functin that returns html of the search results
        //on success out out the results
        $.ajax(
        {
            url: "ajax/ajaxApply.php?id=" + id + "&sitter=" + "<?php echo $user->uid ?>",
            type: "get",
            success: outputJobApply,
            error: function(obErr,obStat,txtMsg) {alert(obErr)}
        });
    };
	
    var outputJobList = function (sResults)
    {
        //print the passed in results into the results pane
        //document.getElementById('searchResults').innerHTML = sResults;
		$("#joblist").html(sResults);
		
		$(".jobli").bind("click", function(event) {
            ajaxDetails($(this).attr("id"));
        });
    };
    
	var outputJobDetails = function (sResults)
    {
		$("#jobdetails").html(sResults);
		
		$(".jobapp").bind("click", function(event) {
            ajaxApply($(this).attr("id"));
        });
    };
	
	var outputJobApply = function (sResults)
	{
		$("#applied").html(sResults);
	};
	
    var dateChange = function()
    {
	var date = $("#date").attr("value");
        
        ajaxDate(date);
    };
    
    $("document").ready(function() {

        $("#date").bind("change", function(event) {
            dateChange();
        });
    });       
</script>


<table border='0' cellspacing='0' width='100%'>
<colgroup style="width: 250px;"></colgroup>
</colgroup>
    <tr>
        <td valign='top' height='1' style='border-bottom: 1px solid black; padding:  0 20px 20px 0;'>
<?php
    $db = new dbObject();
    $qryJobs = $db->selectQuery("DISTINCT date", "finJobs", "date >= CURDATE()", "date");
    
    $frmDate = "<form id='frmDate' name='frmDate'>";
    $frmDate .= "<label for='date'>Date </label><select id='date' name='date'>";
	$frmDate .= "<option style='display: none;'>Select a Date</option>";
    for($i = 0; $i < $qryJobs->num_rows; $i++)
    {
        $arr = $qryJobs->fetch_assoc();
        $rawday = $arr['date'];
        $day = @date("F jS, Y", strtotime($arr['date']));
        $frmDate .= "<option value='$rawday'>$day</option>";
    }
    $frmDate .= "</select></form>";
    echo $frmDate;
?>
        </td>
        <td rowspan='2' valign='top' style='border-left: 1px solid black; padding:  0  20px 20px 20px;'>
            <div id='jobdetails'></div>
        </td>
    </tr>
    <tr>
        <td valign='top' style='padding:  20px 20px 0 0;'>
            <div id='joblist'></div>
        </td>
    </tr>
</table>

<?php
	}
	else //not logged in or not a sitter
	{
		echo "<p class='err'>You must be logged in as a babysitter to use this page</p>";
	}
?>

<?php
    function __autoload($sClassName)
    {
        require_once("classes/$sClassName.class.php");        
    }
?>
<?php
    $sTitle = "Browse Jobs";
    
    require_once("css/wrapper.php");
?>